PREFIX=$(HOME)/kp-install/
mkdir $PREFIX
cd $PREFIX
wget http://newos.org/toolchains/x86_64-elf-4.9.1-Linux-x86_64.tar.xz
tar xf x86_64-elf-4.9.1-Linux-x86_64.tar.xz
rm x86_64-elf-4.9.1-Linux-x86_64.tar.xz
echo export PATH=$(pwd)/x86_64-elf-4.9.1-Linux-x86_64/bin/:$PATH >> .settings
echo export GCCPREFIX=x86_64-elf- >> .settings
echo "Execute 'source .settings' before using the framework"
